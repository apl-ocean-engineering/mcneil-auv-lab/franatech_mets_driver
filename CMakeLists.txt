cmake_minimum_required(VERSION 3.0.2)
project(franatech_mets_driver)

find_package(catkin REQUIRED COMPONENTS
  apl_msgs
  franatech_msgs
  rospy
)

catkin_python_setup()

catkin_package(
  CATKIN_DEPENDS apl_msgs franatech_msgs rospy
)

catkin_install_python(PROGRAMS
  nodes/franatech_mets_driver
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

catkin_add_nosetests(test)
