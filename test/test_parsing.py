import unittest
import numpy as np

from franatech_mets_driver import MetsCalibParams, MetsParser
import franatech_msgs.msg


class TestParsing(unittest.TestCase):
    # Example of valid string from Craig's documentation
    docs_example = ">1:0AF2 2:07FB 3:0714 4:0000 5:0000 6:0000 7:0000 8:0000 \r".encode(
        "utf-8"
    )
    # Example of data actually returned by the device
    mets_example = ">1:032E 2:0575 3:00B2 4:032E 5:032E 6:032E 7:032E 8:032E".encode(
        "utf-8"
    )

    def setUp(self):
        # Hardcoding params from documentation

        params = MetsCalibParams(
            -0.108, 2.672, 0.692, -15.684, 21.842, 6.710, 1.626, 22.50, -4.72
        )
        self.parser = MetsParser(params)

    def test_good_input(self):
        for data in [self.docs_example, self.mets_example]:
            msg = self.parser.parse_bytes(data)
            self.assertIsInstance(msg, franatech_msgs.msg.Mets)

    def test_invalid_bytes(self):
        """
        0x9c, 0xa5 Are both examples of invalid unicode bytes

        Insert an undecodeable byte at a random position of the input, confirm
        that the parser catches the error.
        """
        for _ in np.arange(5):
            bad_bytearray = bytearray(self.docs_example)
            idx = np.random.randint(len(bad_bytearray))
            bad_bytearray[idx] = 0x9C
            bad_input = bytes(bad_bytearray)
            msg = self.parser.parse_bytes(bad_input)
            self.assertIsNone(msg)

    def test_too_few_tokens(self):
        # simply removed single field from docs_example
        input_data = "1:0AF2 2:07FB 3:0714 5:0000 6:0000 7:0000 8:0000\n".encode(
            "utf-8"
        )
        msg = self.parser.parse_bytes(input_data)
        self.assertIsNone(msg)


if __name__ == "__main__":
    unittest.main()
