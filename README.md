## franatech_mets_driver

Driver for a Franatech METS methane sensor, using the RS232 interface.

This is a pretty simple instrument -- There's no configuration, so we simply poll
it for data and publish the parsed result.

(It seems that a newer version has a mode for "stream continuously", which has to be
reset if the sensor is power cycled, but ours doesn't respond to that command.)


Parameters:
* port: serial port to connect to
* rate_hz: rate at which to query data.
* A0-A2, B0-B2, D, T1-T2: calibration parameters provided by Franatech


Possible enhancements to node:
* Buffer data so messages can be reconstructed if a read times out. I didn't bother with that to start with because it makes the code more complicated, and if the serial timeout is set to greater than 1Hz, I wouldn't ever expect a message to be truncated if the sensor is operating as expected.
