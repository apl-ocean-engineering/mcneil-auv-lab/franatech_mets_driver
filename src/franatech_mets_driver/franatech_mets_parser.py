import numpy as np
import re
import rospy
from typing import Optional, NamedTuple

import franatech_msgs.msg

MetsCalibParams = NamedTuple(
    "MetsCalibParams",
    [
        ("a0", float),
        ("a1", float),
        ("a2", float),
        ("b0", float),
        ("b1", float),
        ("b2", float),
        ("d", float),
        ("t1", float),
        ("t2", float),
    ],
)


class MetsParser:
    def __init__(self, params: MetsCalibParams) -> None:
        # Expected data format. Entries 3-8 are "reserved"
        self.data_pattern = r">1:(?P<methane_counts>[0-9A-F]{4}) 2:(?P<temperature_counts>[0-9A-F]{4}) 3:[0-9A-F]{4} 4:[0-9A-F]{4} 5:[0-9A-F]{4} 6:[0-9A-F]{4} 7:[0-9A-F]{4} 8:[0-9A-F]{4}"
        self.params = params

    def calc_temperature(self, temperature_volts: float) -> float:
        return temperature_volts * self.params.t1 + self.params.t2

    def calc_concentration(
        self, temperature_volts: float, methane_volts: float
    ) -> float:
        """
        Calculate methane concentration (in micro mol per liter) from
        measurements of methane and temperature voltage.
        """
        temp_term_a = self.params.a0 + self.params.a1 * np.exp(
            -1 * temperature_volts / self.params.a2
        )
        temp_term_b = self.params.b0 + self.params.b1 * np.exp(
            -1 * temperature_volts / self.params.b2
        )
        with np.errstate(invalid="raise"):
            try:
                cc: float = np.exp(
                    self.params.d
                    * np.log(
                        temp_term_a * ((1.0 / methane_volts) - (1.0 / temp_term_b))
                    )
                )
            except FloatingPointError as ex:
                rospy.logwarn("Exception in calc_concentration: {}".format(ex))
                # This will happen if we have invalid values in log
                cc = -1
        return cc

    def parse_bytes(self, data: bytes) -> Optional[franatech_msgs.msg.Mets]:
        # Try to parse received bytes into data
        try:
            data_str = data.decode("utf-8")
        except UnicodeDecodeError as ex:
            rospy.logwarn("Data not valid unicode: {}".format(ex))
            return None
        mm = re.match(self.data_pattern, data_str)
        if mm is None:
            rospy.logwarn("Unable to parse response from sensor: {}".format(data_str))
            return None

        # Convert data from counts into science units
        temperature_counts = int(mm.group("temperature_counts"), 16)
        methane_counts = int(mm.group("methane_counts"), 16)
        temperature_volts = 5.0 * temperature_counts / 4096
        methane_volts = 5.0 * methane_counts / 4096
        temperature = self.calc_temperature(temperature_volts)
        methane_concentration = self.calc_concentration(
            temperature_volts, methane_volts
        )

        # Publish parsed data to the mets topic
        mets_msg = franatech_msgs.msg.Mets()
        mets_msg.methane_is_voltage = True
        mets_msg.temperature_counts = temperature_counts
        mets_msg.temperature = temperature
        mets_msg.methane_counts = methane_counts
        mets_msg.methane_concentration = methane_concentration
        return mets_msg
