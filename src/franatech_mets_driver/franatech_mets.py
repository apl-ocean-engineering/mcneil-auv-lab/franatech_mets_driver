#! /usr/bin/env python3
"""
Driver for the version of a Franatech METS sensor which:
* Reports methane concentration via digitized voltage
* does NOT support continuous mode; each measurement is triggered via "#A0\r"
"""
import rospy

import apl_msgs.msg
import franatech_msgs.msg
import serial

from .franatech_mets_parser import MetsParser, MetsCalibParams


class FranatechMetsDriver:
    def __init__(self) -> None:
        self.setup_parameters()

        self.mets_parser = MetsParser(self.calib_params)

        self.raw_pub = rospy.Publisher("~raw", apl_msgs.msg.RawData, queue_size=1)
        self.mets_pub = rospy.Publisher("~mets", franatech_msgs.msg.Mets, queue_size=1)

        # There are no options to change the sensor's baudrate
        baudrate = 9600
        # When in continuous measurement mode, the sensor reports data at ~1 Hz
        serial_timeout = 0.5
        self.serial = serial.Serial(
            port=self.port, baudrate=baudrate, timeout=serial_timeout
        )
        # Termination character for messages from the sensor. Will be carriage return.
        self.termination = bytes([0x0D])
        # Command to request data from the sensor
        self.data_command = bytes("#A0\r", "utf-8")

    def setup_parameters(self) -> None:
        self.port = rospy.get_param("~port")
        self.rate_hz = rospy.get_param("~rate_hz")

        # Calibration parameters provided by Franatech
        # These are for the formulae accepting: temperature(volts), methane(volts)
        a0: float = rospy.get_param("~A0")
        a1: float = rospy.get_param("~A1")
        a2: float = rospy.get_param("~A2")
        b0: float = rospy.get_param("~B0")
        b1: float = rospy.get_param("~B1")
        b2: float = rospy.get_param("~B2")
        dd: float = rospy.get_param("~D")
        t1: float = rospy.get_param("~T1")
        t2: float = rospy.get_param("~T2")
        self.calib_params = MetsCalibParams(a0, a1, a2, b0, b1, b2, dd, t1, t2)

    def send_data_request(self) -> None:
        raw_msg = apl_msgs.msg.RawData()
        raw_msg.header.stamp = rospy.Time.now()
        raw_msg.direction = raw_msg.DATA_OUT
        raw_msg.data = self.data_command
        self.raw_pub.publish(raw_msg)

        self.serial.write(self.data_command)

    def maybe_parse_data(self) -> None:
        """
        Should be called directly after send_data_request.
        Will attempt to read data from the instrument; in case of failure,
        simply returns.
        * If any bytes are received, they well be published as a RawData.msg
        * If the bytes are parseable, a FranatechMets.msg will be published
        """
        data = self.serial.read_until(self.termination)
        if len(data) == 0:
            rospy.loginfo("Mets driver: Serial read timed out.")
            return

        # Publish received data to the raw topic
        data_stamp = rospy.Time.now()
        raw_msg = apl_msgs.msg.RawData()
        raw_msg.header.stamp = data_stamp
        raw_msg.direction = raw_msg.DATA_IN
        raw_msg.data = data
        self.raw_pub.publish(raw_msg)

        mets_msg = self.mets_parser.parse_bytes(data)
        if mets_msg is not None:
            mets_msg.header.stamp = data_stamp
            self.mets_pub.publish(mets_msg)

    def run(self) -> None:
        # First, clear stale data
        data = self.serial.read_until(self.termination)
        while len(data) > 0:
            data = self.serial.read_until(self.termination)

        rr = rospy.Rate(self.rate_hz)
        while not rospy.is_shutdown():
            try:
                self.send_data_request()
                self.maybe_parse_data()
            except Exception as ex:
                rospy.logerr("Unhandled Exception! {}".format(ex))
            rr.sleep()


def main() -> None:
    rospy.init_node("franatech_mets_driver")
    fm = FranatechMetsDriver()
    fm.run()
